let maine = document.getElementById("GruppenErstellen");

let cat = document.getElementById("cat");
let Name = document.getElementById("Gruppenname");
let desc = document.getElementById("beschreibung");
let numb = document.getElementById("Mitgliederanzahl");
let grenze = document.getElementById("begrenzteMitglieder");
let freizeit = document.getElementsByClassName("freizeit");
let abgabe = document.getElementById("abgabe");
let veranstaltungen = document.getElementById("veranstaltungen");

let eingeladen = document.getElementById("eingeladen");
let einladen = document.getElementById("einladen");
let userSearch = document.getElementById("userSearch");

let own = document.getElementById("own");
let isFreizeit = false;
let isOwn = false;
let sub = document.getElementById("weiter");
let checkBoxes = document.getElementsByClassName("freizeitBox");
let checkedTitle = document.getElementById("checkBoxH3");

function init() {
    hideAll();
    handleChange();
    cat.addEventListener("change", handleChange);
    grenze.addEventListener("change", begrenzteMitglieder);
    veranstaltungen.addEventListener("change", eigeneVeranstaltung);
    sub.addEventListener("click", tryToSub);
    Name.addEventListener("change", resetStyleEvent);
    desc.addEventListener("change", resetStyleEvent);
    numb.addEventListener("change", resetStyleEvent);
    own.addEventListener("change", resetStyleEvent);
    einladen.addEventListener("click", mitgliedEinladen);
}

function hideAll() {
    maine.style.display = "none";
    numb.style.display = "none";
    for (var i = 0; i < freizeit.length; i++) {
        freizeit[i].style.display = "none";
    }
    abgabe.style.display = "none";
}

function handleChange() {
    hideAll();
    resetStyle(cat);
    grenze.checked = false;
    switch (cat.value) {
        case "null":
            break;

        case "frei":
            maine.style.display = "";
            for (var i = 0; i < freizeit.length; i++) {
                freizeit[i].style.display = "";
            }
            isFreizeit = true;
            break;

        default:
            maine.style.display = "";
            abgabe.style.display = "";
            eigeneVeranstaltung();
            isFreizeit = false;
    }
}

function begrenzteMitglieder() {
    if (grenze.checked) {
        numb.style.display = "";
    } else {
        numb.style.display = "none";
    }
}

function tryToSub() {
    let gName;
    let gDesc;
    let gNum;
    let gCat;
    let complete = true;
    if (Name.value.length == 0) {
        setAsMissing(Name);
        complete = false;
    }
    else {
        complete = complete && true;
        gName = Name.value;
    }

    if (cat.value == "null") {
        setAsMissing(cat);
        complete = false;
    }

    if (desc.value.length == 0) {
        setAsMissing(desc);
        complete = false;
    }
    else {
        complete = complete && true;
        gDesc = desc.value;
    }

    if (grenze.checked) {
        if (numb.value <= 0) {
            setAsMissing(numb);
            complete = false;
        }
    }
    
    else {
        gNum = numb.value;
    }

    if (!isFreizeit) {
        if (isOwn) {
            if (own.value.length == 0) {
                setAsMissing(own);
                complete = false;
            }
        }
        else if (veranstaltungen.value == "null") {
            setAsMissing(veranstaltungen);
            complete = false;
        }
    } else {
        if (!atLeastOneChecked()) {
            checkedTitle.style.display =  "";
            complete = false;
        } else {
            checkedTitle.style.display = "none";
        }
    }
    if (complete) {
        window.localStorage.setItem("erstellt", gName);
        window.location.href = "../StartSeite/StartSeite.html";
    }
}


function atLeastOneChecked(elements) {
    for (var i = 0; i < checkBoxes.length; i++) {
        if (checkBoxes[i].checked) {
            return true;
        }
    }
    return false;
}


function eigeneVeranstaltung() {
    resetStyle(veranstaltungen);
    if (veranstaltungen.value == "own") {
        own.previousElementSibling.previousElementSibling.style.display = "";
        own.style.display = "";
        isOwn = true;
    }
    else {
        own.previousElementSibling.previousElementSibling.style.display = "none";
        own.style.display = "none";
        isOwn = false;
    }
}


// Fügt den in userSearch eingegebenen Nutzer in die Liste eingeladen hinzu
function mitgliedEinladen() {
    Name = userSearch.value;
    if (Name.length < 1) {
        setAsMissing(userSearch);
    } else {
        resetStyle(userSearch);
        userSearch.value = "";
        eingeladen.appendChild(neuesMitglied(Name));
    }
}

// Gibt einen Listeneintrag für die "eingeladen" Liste zurück. Diese enthält den Namen
// und einen Button zum löschen.
function neuesMitglied(Name) {
    member = document.createElement("li");
    nameTag = document.createTextNode(Name);
    member.appendChild(nameTag);

    member.appendChild(document.createTextNode(" "));

    button = document.createElement("button");
    button.appendChild(document.createTextNode("X"));
    button.addEventListener("click", mitgliedEntfernen);
    member.appendChild(button);

    return member;
}


function mitgliedEntfernen() {
    li = event.target.parentNode;
    li.parentNode.removeChild(li);
}

function resetStyleEvent () {
    resetStyle(event.target);
}


function resetStyle(element) {
    element.classList.remove("missing");
}


function setAsMissing(element) {
    element.classList.add("missing");
}



document.addEventListener("DOMContentLoaded", init);
