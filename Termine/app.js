

/* Auslesen des heutigen Tag (d)/Monat (m)/Jahr (y)*/
/*
y = n.getFullYear();
m = n.getMonth() + 1;
d = n.getDate();
document.getElementById("date").innerHTML = m + "/" + d + "/" + y;
document.getElementsByClassName("year").innerHTML= y;
document.getElementsByClassName("months") = m;
*/
var woche = ['Sonntag','Montag','Dienstag','Mittwoch','Donnerstag','Freitag','Samstag']
var Monat = ['Jan','Feb','Mar','Apr','May','Jun','July','Aug','Sep','Oct','Nov','Dec']
var day = ['SUN','MON','TUE','WED','THU','FRI','SAT']

 n =  new Date();
 m = n.getMonth();
 d = n.getDate();
 y = n.getFullYear();
 var bearbeiten = false;

 var alter_titel;
 var altes_datum;
 var alte_kategorie;

function tag() {
    
    wochentag = n.getDay();
    dstring = woche[wochentag];
    document.getElementById('num-date').innerHTML= d;
    document.getElementById('date-string').innerHTML= dstring;
    document.getElementById('year').innerHTML= y;

}



function monat() {
    
    wochentag = n.getDay();
    dstring = day[wochentag];
    mstring = Monat[m];
    document.getElementById(mstring).style.backgroundColor = "green";
    document.getElementById(dstring).style.backgroundColor = "green";
    document.getElementById(d).style.backgroundColor = "green"    
    
}

function calender() {

    n =  new Date();
    m = n.getMonth();
    d = n.getDate();
    y = n.getFullYear();

    a = new Date(y, m, 0);
    k = 1;
    k1 = 1;
    wochentag = a.getDay();
    dstring = day[wochentag];
    tageimMonat = (new Date(y,m+1,0).getDate());
    console.log(a)
    console.log(m)
    var week = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    for (i = 0; i < week.length; i++) {
        if (i < day.indexOf(dstring)) {
            diff = i - day.indexOf(dstring) + 1;
            week[i] = (new Date(y,m,diff).getDate());;
        } else {
            week[i] = k  ;
            
            if (k > tageimMonat) {
                week[i] = k1
                k1++
            }
            k++
        }
    }
    var week1 = [0, 0, 0, 0, 0, 0, 0]
    var week2 = [0, 0, 0, 0, 0, 0, 0]
    var week3 = [0, 0, 0, 0, 0, 0, 0]
    var week4 = [0, 0, 0, 0, 0, 0, 0]
    var week5 = [0, 0, 0, 0, 0, 0, 0]
    var week6 = [0, 0, 0, 0, 0, 0, 0] 
    
        week1 = week.slice(0,7);
        week2 = week.slice(7,14);
        week3 = week.slice(14,21);
        week4 = week.slice(21,28);
        week5 = week.slice(28,35);
        week6 = week.slice(35,42);
    
        for (i = 0; i < week1.length; i++) {
            var y  = document.createElement("div");
            var y1  = document.createElement("div");
            var y2  = document.createElement("div");
            var y3  = document.createElement("div");
            var y4  = document.createElement("div");
            var y5  = document.createElement("div");
            
            y.innerHTML = week1[i];
            y1.innerHTML = week2[i];
            y2.innerHTML = week3[i];
            y3.innerHTML = week4[i];
            y4.innerHTML = week5[i];
            y5.innerHTML = week6[i];

            
            document.getElementById('first-week').appendChild(y);
            document.getElementById('second-week').appendChild(y1);
            document.getElementById('third-week').appendChild(y2);
            document.getElementById('fourth-week').appendChild(y3);
            document.getElementById('fifth-week').appendChild(y4);
            document.getElementById('sixth-week').appendChild(y5);
            y.classList.add("datumzahl");
            y1.classList.add("datumzahl");
            y2.classList.add("datumzahl");
            y3.classList.add("datumzahl");
            y4.classList.add("datumzahl");
            y5.classList.add("datumzahl");

            if (i < week1.indexOf(1) ) {
                y.setAttribute("id", y.innerHTML + "alt")
            } else {
                y.setAttribute("id", y.innerHTML)
            }
            y1.setAttribute("id", y1.innerHTML)
            y2.setAttribute("id", y2.innerHTML)
            y3.setAttribute("id", y3.innerHTML)
            y4.setAttribute("id", y4.innerHTML)
            y5.setAttribute("id", y5.innerHTML)
            
            
        }    
        console.log(document.getElementById('first-week') )


            

}


function savevalue(filter) {
    document.getElementById('termindropbtn').value = filter;

}

function terminspeichern() {
    if (bearbeiten) {
        window.removeEventListener("beforeunload", terminspeichern);
        bearbeiten = false;
    }

    a = document.getElementById('termintitel').value;
    document.getElementById('termintitel').value = "";
    b = document.getElementById('termindatum').value;
    document.getElementById('termindatum').value = "";
    c = document.getElementById('termindropbtn').value;
    document.getElementById('termindropbtn').value = "";

    if (a.length == "") {
        
        document.getElementById('termintitel').placeholder = "Bitte Titel eingeben "
    } else {
        if (b.length == "") {
            
            document.getElementById('termindatum').placeholder = "Kein Datum ausgewählt "
        } else {
            if (c.length == "") {
                
                document.getElementById('termindropbtn').placeholder = "Bitte Kategorie wählen"
            } else {


        d = b.substr(-2)
        d1 = b.substr(-5, 2);
    
        
    
        var y= document.createElement("div");
        var ytitel= document.createElement("div");
        var ydatum= document.createElement("div");
        var weg= document.createElement("img");
        var change = document.createElement("img");
        ytitel.innerHTML = a
        ydatum.innerHTML = b

        change.src = "../Logos/Bearbeiten.svg";
        change.style.height = "4vh";
        change.setAttribute("id", "bearbeiten");
        change.setAttribute("onclick", "bearbeitenFun(this.parentNode, '"+a+"', '"+b+"', '"+c+"', '"+d+"')");
        change.style.cursor = "pointer";

        weg.src = "../Logos/Löschen.svg";
        weg.style.height= "4vh";
        weg.setAttribute("id", "loeschen");
        weg.setAttribute("onclick", "loeschen(this.parentNode, '"+d+"', '"+c+"')");
        weg.style.cursor = "pointer";
    
        y.appendChild(ytitel);
        y.appendChild(ydatum);
        y.appendChild(change);
        y.appendChild(weg);

        document.getElementById('Veranstaltungen').appendChild(y);

        
        
        
        
        
        y.classList.add(c);
        y.setAttribute("id", c);
        if ( d1-1 == m ) {
            document.getElementById(d).classList.add(c);
        }
    }
}


    }
    

    
}

function resetWerte() {
    document.getElementById('termintitel').value = alter_titel;
    alter_titel = "";

    document.getElementById('termindatum').value = altes_datum;
    altes_datum = "";

    document.getElementById('termindropbtn').value = alte_kategorie;
    alte_kategorie = "";

    terminspeichern();
}

function ausklappen() {
    if (bearbeiten) {
        resetWerte();
    }
    if (document.getElementById('terminerstellen').style.display == "block") {
        document.getElementById('terminerstellen').style.display = "none";
    }
        else {
            document.getElementById('terminerstellen').style.display = "block";
        }
}

function loeschen(element,d, c) {
    element.parentNode.removeChild(element);
    document.getElementById(d).classList.remove(c);
}

function bearbeitenFun(element, a, b, c, d) {
    if (bearbeiten) {
        resetWerte();
    }
    alter_titel = a;
    document.getElementById('termintitel').value = a;

    altes_datum = b;
    document.getElementById('termindatum').value = b;

    alte_kategorie = c;
    document.getElementById('termindropbtn').value = c;

    bearbeiten = true;

    loeschen(element, d, c);
    if (document.getElementById('terminerstellen').style.display != "block") {
        ausklappen();
    }
    window.addEventListener("beforeunload", resetWerte);
}