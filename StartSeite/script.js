let GruppenListe = document.getElementById("Gruppentabelle");
let overview = document.getElementById("over");
let my = document.getElementById("self");
let sent = document.getElementById("sent");

function addNewGroup (Name, type) {
    let letzteReihe = $("#Gruppentabelle tr:last");
    let neueReihe = letzteReihe[0].cloneNode(true);
    if (Name == "MC Paartherapeuten")   {
        let link = document.createElement("a");
        link.innerHTML = "MC Paartherapeuten";
        link.href = "../Gruppe beitreten/Gruppeninfos.html";
        neueReihe.firstChild.nextSibling.innerHTML = "";
        neueReihe.firstChild.nextSibling.appendChild(link);
    } else {
        neueReihe.firstChild.nextSibling.innerHTML = Name;
    }
    neueReihe.style.diplay = "none";
    neueReihe.setAttribute("class", "");
    for (let i = 0; i < type.length; i++) {
        neueReihe.classList.add(type[i]);
    }
    letzteReihe[0].parentNode.appendChild(neueReihe);
}

function init () {
    if (window.localStorage.getItem("Paartherapeuten") == "true") {
        addNewGroup("MC Paartherapeuten", ["sent"]);
    }
    let gName = window.localStorage.getItem("erstellt");
    if (gName != null) {
        addNewGroup(gName, ["over", "self"]);
    }
    overview.addEventListener("click", changeFilter);
    my.addEventListener("click", changeFilter);
    sent.addEventListener("click", changeFilter);
    showGroupsOfType("over");
}


function changeFilter() {
    overview.classList.remove("selected");
    my.classList.remove("selected");
    sent.classList.remove("selected");
    let value = event.target.id;
    let target = event.target;
    target.classList.add("selected");
    showGroupsOfType(value);
}

function showGroupsOfType(value) {
    let Gruppen = $("#Gruppentabelle tr");
    for (let i = 1; i < Gruppen.length; i++) {
        let aktuelleGruppe = Gruppen[i];
        if (aktuelleGruppe.classList.contains(value)) {
            aktuelleGruppe.style.display = "";
        } else {
            aktuelleGruppe.style.display = "none";
        }
    }
}

document.addEventListener("DOMContentLoaded", init);