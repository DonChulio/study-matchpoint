let id = document.getElementById("id");
let pw = document.getElementById("password");
let remember = document.getElementById("remember");

let idLabel = document.getElementById("idLabel");
let sso = document.getElementById("sso");
let user = document.getElementById("user");
let state = "sso";


// Prüft, ob die Inputs alle stimmen. Wenn dem so ist, wird die Startseite geöffnet.

function sub() {
    let correct = true;

    // Teste, ob die ID eingegeben wurde.
    if (id.value.length == 0) {
        correct = false;
        id.style.border = "1px solid rgb(245, 22, 22)";
        id.style.color = "rgb(245, 22, 22)";
        if (state == "sso") {
            id.placeholder = "LUH-ID erforderlich !"
        } else {
            id.placeholder = "Benutzername erforderlich!"
        }
    }

    // Teste, ob das Password eingegeben wurde.
    if (pw.value.length == 0) {
        correct = false;
        pw.style.border = "1px solid rgb(245, 22, 22)";
        pw.style.color = "rgb(245, 22, 22)";
        pw.placeholder = "Passwort erforderlich !"
    }

    // Speichern der Login Daten. Bisher nur ID
    if (remember.checked == true) {
        window.localStorage.setItem("studymatchid", id.value);
    }
    else {
        window.localStorage.removeItem("studymatchid");
    }

    // Öffnet den Link wenn alles correct ist
    if (correct) {
        window.location.href = "../StartSeite/StartSeite.html";
    }
}

function reset (target) {
    let object = document.getElementById(target);
    object.style.border = "1px solid #ccc";
    object.style.color = "";
    if (target == "id") {
        if (state == "sso") {
            object.placeholder = "z.B. 1a2-b3c";
        } else {
            object.placeholder = "Dein Benutzername...";
        }
    }
    else {
        object.placeholder = "Dein Passwort...";
    }
}


function setLogin() {
    choice = event.target;
    reset("id");
    reset("password");  // Damit man nicht nach einer Falschauswahl wechselt und rote Felder bekommt

    if (choice.id == "sso") {
        state = "sso";
        id.placeholder = "z.B. 1a2-b3c";
        idLabel.innerHTML = "LUH-ID";

        befor = user;
    } else {
        state = "user";
        id.placeholder = "Dein Benutzername...";
        idLabel.innerHTML = "Benutzername";

        befor = sso;
    }
    id.value = "";

    befor.classList.remove("selected");
    befor.addEventListener("click", setLogin);

    choice.classList.add("selected");
    choice.removeEventListener("click", setLogin);
}


function loadID () {
    user.addEventListener("click", setLogin);
    if (window.localStorage.getItem("studymatchid") != null) {
        id.value = window.localStorage.getItem("studymatchid");
    }
    window.localStorage.removeItem("erstellt");
    window.localStorage.removeItem("Paartherapeuten");
}