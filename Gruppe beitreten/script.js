let keineGruppen = document.getElementById("keineGruppen");
let gruppenGmci = document.getElementById("gruppenGmci");
let abgabe = document.getElementById("abgabe");
let cat = document.getElementById("kategorie");
let freizeit = document.getElementById("freizeit");
let lernen = document.getElementById("lernen");
let erstellen = document.getElementById("erstellen");

function init() {
    hideAll();
    cat.addEventListener("change", handleCat);
    freizeit.addEventListener("change", handleOthers);
    abgabe.addEventListener("change", handleAbgabe);
    lernen.addEventListener("change", handleOthers);
    erstellen.addEventListener("click", zuErstellen);
    gruppenGmci.style.display = "none";
}


function handleCat() {
    hideAll();
    switch (event.target.value) {
        case "Lernen":
            showSelect(lernen);
            break;

        case "Abgabe":
            showSelect(abgabe);
            break;

        case "Freizeit":
            showSelect(freizeit);
            break;
    }
}

function handleAbgabe() {
    hideGroups();
    if (abgabe.value == "GMCI") {
        gruppenGmci.style.display = "block";
    }
    else if (abgabe.value != "") {
        keineGruppen.style.display = "block";
    }
}

function handleOthers() {
    hideGroups();
    if (event.target.value != "") {
        keineGruppen.style.display = "block";
    }
}

function hideAll() {
    keineGruppen.style.display = "none";
    abgabe.parentNode.style.display = "none";
    gruppenGmci.style.display = "none";
    freizeit.parentNode.style.display = "none";
    lernen.parentNode.style.display = "none";
}

function zuErstellen() {
    window.location.href = "../Gruppe erstellen/Gruppe_erstellen.html";
}

function hideGroups() {
    keineGruppen.style.display = "none";
    gruppenGmci.style.display = "none";
}

function showSelect(element) {
    hideAll();
    element.parentNode.style.display = "block";
    element.value = "";
}

document.addEventListener("DOMContentLoaded", init);
