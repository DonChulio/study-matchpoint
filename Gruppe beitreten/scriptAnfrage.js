let input = document.getElementById("input");
let weiter = document.getElementById("send");
let abbrechen = document.getElementById("ex");

function init() {
    input.addEventListener("change", resetStyleEvent);
    weiter.addEventListener("click", tryToSub);
    abbrechen.addEventListener("click", exit)
}

function tryToSub() {
    let complete = true;
    if (input.value.length == 0) {
        setAsMissing(input);
        complete = false;
    }
    if (complete) {
        window.localStorage.setItem("Paartherapeuten", "true");
        window.location.href = "../StartSeite/StartSeite.html";
    }
}

function exit() {
    window.location.href = "Gruppe_suchen.html";
}

function resetStyleEvent () {
    resetStyle(event.target);
}

function resetStyle(element) {
    element.classList.remove("missing");
}

function setAsMissing(element) {
    element.classList.add("missing");
}



document.addEventListener("DOMContentLoaded", init);