function eintragen() {
    var a = document.getElementById('inputStudiengang').value;
    console.log(a)
    localStorage.setItem("inputStud", a);
    console.log(localStorage)
    

}

function loadthis() {
    document.getElementById('bearbeiten').style.display = "none";
    console.log(sessionStorage)
    if (sessionStorage.length == 0) {
        sessionStorage.setItem("inputnickname", "SunnyShinySlimy");
        sessionStorage.setItem("inputvorname", "Moritz");
        sessionStorage.setItem("inputnachname", "Mustermann");
        sessionStorage.setItem("inputstudiengang", "Maschinenbau");
        sessionStorage.setItem("inputarbeit", "Kassierer bei MC Donalds");
        sessionStorage.setItem("inputwohnort", "Nürnberg");
        sessionStorage.setItem("inputalter", "12");
        sessionStorage.setItem("inputbeziehungsstatus", "Single");
        sessionStorage.setItem("inputemail", "a.360noscope@gmx.de");
    }
    console.log(sessionStorage)
    console.log(localStorage)

    displayIfInput("inputvorname", "vorname");

    displayIfInput('inputnachname', 'nachname');

    displayIfInput('inputstudiengang', 'studiengang');
    
    displayIfInput('inputarbeit', 'arbeit');
    
    displayIfInput('inputwohnort', 'wohnort');

    displayIfInput('inputalter', 'alter');

    displayIfInput('inputbeziehungsstatus', 'beziehungsstatus');

    displayIfInput('inputemail', 'email');

}

function speichern() {
    if (ausgaben()) {
        document.getElementById('bearbeiten').style.display = "none";
        document.getElementById('anschauen').style.display = "block";
        document.getElementById('sidebearbeiten').classList.remove('selected');
        document.getElementById('sidemeinprofil').classList.add('selected');
        document.getElementById('speichern').style.display = "none";
        check();
    }
}

function gotobearbeiten() {
    document.getElementById('bearbeiten').style.display = "block";
    document.getElementById('anschauen').style.display = "none";
    document.getElementById('speichern').style.display = "block";
    
    document.getElementById('sidebearbeiten').classList.add('selected');
    document.getElementById('sidemeinprofil').classList.remove('selected');
    eingaben();
}

function eingaben() {
    document.getElementById('inputnickname').value = document.getElementById('nickname').innerHTML;
    document.getElementById('inputvorname').value = document.getElementById('vorname').innerHTML;
    document.getElementById('inputnachname').value = document.getElementById('nachname').innerHTML;
    document.getElementById('inputstudiengang').value = document.getElementById('studiengang').innerHTML;
    document.getElementById('inputarbeit').value = document.getElementById('arbeit').innerHTML;
    document.getElementById('inputwohnort').value = document.getElementById('wohnort').innerHTML;
    document.getElementById('inputalter').value = document.getElementById('alter').innerHTML;
    document.getElementById('inputbeziehungsstatus').value = document.getElementById('beziehungsstatus').innerHTML;
    document.getElementById('inputemail').value = document.getElementById('email').innerHTML;
    a= document.getElementsByClassName('wassucheich')
    for (i = 0; i < a.length; i++) {
        document.getElementsByClassName('inputwassucheich')[i].value = a[i].innerHTML;
      }
    b= document.getElementsByClassName('hobbies')
    for (j = 0; j < b.length; j++) {
          document.getElementsByClassName('inputhobbies')[j].value = b[j].innerHTML;
    }  
    
    document.getElementById('inputbiographie').value = document.getElementById('biographie').innerHTML;

}

function storeAndDisplayIfInput (inputID, elementID) {
    sessionStorage.setItem(inputID, document.getElementById(inputID).value);
    displayIfInput(inputID, elementID);
}

function displayIfInput (inputID, elementID) {
    if (sessionStorage.getItem(inputID).length != 0) {
        document.getElementById(elementID).parentNode.style.display = "";
        document.getElementById(elementID).innerHTML = sessionStorage.getItem(inputID);
    } else {
        document.getElementById(elementID).parentNode.style.display = "none";
        document.getElementById(elementID).innerHTML = sessionStorage.getItem(inputID);
    }
}

function ValidateEmail(mail) 
{
 if (/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(mail))
  {
    return (true)
  }
    // alert("You have entered an invalid email address!")
    return (false)
}

function ausgaben() {
    let wrongInput = false;

    if (!ValidateEmail(document.getElementById("inputemail").value)) {
        setAsMissing(document.getElementById("inputemail"));
        wrongInput = true;
    }

    resetStyle(document.getElementById("inputemail"));
    storeAndDisplayIfInput('inputemail', 'email');

    if (document.getElementById("inputnickname").value.length == 0) {
        setAsMissing(document.getElementById("inputnickname"));
        return true;
    }

    if (wrongInput) {
        return false;
    }

    resetStyle(document.getElementById("inputnickname"));
    storeAndDisplayIfInput("inputnickname", "nickname");

    storeAndDisplayIfInput("inputvorname", "vorname");
    
    storeAndDisplayIfInput('inputnachname', 'nachname');

    storeAndDisplayIfInput('inputstudiengang', 'studiengang');
    
    storeAndDisplayIfInput('inputarbeit', 'arbeit');
    
    storeAndDisplayIfInput('inputwohnort', 'wohnort');

    storeAndDisplayIfInput('inputalter', 'alter');

    storeAndDisplayIfInput('inputbeziehungsstatus', 'beziehungsstatus');

    a= document.getElementsByClassName('inputwassucheich')
    for (i = 0; i < a.length; i++) {
        document.getElementsByClassName('wassucheich')[i].innerHTML = a[i].value;
      }
    b= document.getElementsByClassName('inputhobbies')
    for (j = 0; j < b.length; j++) {
          document.getElementsByClassName('hobbies')[j].innerHTML = b[j].value;
    }  
    document.getElementById('biographie').innerHTML = document.getElementById('inputbiographie').value ;

    return true;
}

function resetStyle(element) {
    element.classList.remove("missing");
}


function setAsMissing(element) {
    element.classList.add("missing");
}

function check() {
    if (checkvorname.checked == false) {
        document.getElementById('vornametext').style.display = "block"
    } else { document.getElementById('vornametext').style.display = "none"; }
    if (checknachname.checked == false) {
        document.getElementById('nachnametext').style.display = "block"
    } else { document.getElementById('nachnametext').style.display = "none"; }
    if (checkStudiengang.checked == false) {
        document.getElementById('studiengangtext').style.display = "block"
    } else { document.getElementById('studiengangtext').style.display = "none"; }
    if (checkArbeit.checked == false) {
        document.getElementById('arbeittext').style.display = "block"
    } else { document.getElementById('arbeittext').style.display = "none"; }
    if (checkWohnort.checked == false) {
        document.getElementById('wohnorttext').style.display = "block"
    } else { document.getElementById('wohnorttext').style.display = "none"; }
    if (checkAlter.checked == false) {
        document.getElementById('altertext').style.display = "block"
    } else { document.getElementById('altertext').style.display = "none"; }
    if (checkBeziehungsstatus.checked == false) {
        document.getElementById('beziehungsstatustext').style.display = "block"
    } else { document.getElementById('beziehungsstatustext').style.display = "none"; }
    if (checkemail.checked == false) {
        document.getElementById('emailtext').style.display = "block"
    } else { document.getElementById('emailtext').style.display = "none"; }
        
}